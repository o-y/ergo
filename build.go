package main

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
)

var b = &bytes.Buffer{}

func main() {
	var err error

	cmd := exec.Command("examplgen", []string{"-template", "README.tpl.md", "-code", "v1/tests/readme_test.go"}...)
	cmd.Stderr = os.Stderr

	cmd.Stdout = io.MultiWriter(b, fileoutput("README.md"))

	err = cmd.Run()
	checkerr(err)
}

func checkerr(err error) {
	if err != nil {
		fmt.Println(b.String())
		fmt.Println(err)
		os.Exit(1)
	}
}

func fileoutput(path string) *os.File {
	f, err := os.Create(path)
	checkerr(err)
	return f
}
