package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

// Wrap: Ensure correct error message for plain error
func TestWrap_PlainErr(t *testing.T) {
	err := fmt.Errorf("Error Message")
	E := ergo.Wrap(&err)

	require.Equal(t, "Error Message", E.Error())
	require.Equal(t, "", E.PublicError())
}

// -----------------------------------------------------------------------------

// Wrap: Ensure correct error message for ergo error
func TestWrap_ErgoErr(t *testing.T) {
	var err error
	err = ergo.New("Error Message")
	E := ergo.Wrap(&err)

	require.Equal(t, err, E)
}

// -----------------------------------------------------------------------------

// Wrap: Ensure correct operation with nil
func TestWrap_Nil(t *testing.T) {
	require.Nil(t, ergo.Wrap(nil))

	var err error

	require.Nil(t, ergo.Wrap(&err))
	require.Nil(t, err)
}

// -----------------------------------------------------------------------------

// IsError: Ensure correct operation with nil
func TestIsError_Nil(t *testing.T) {
	require.False(t, ergo.IsError(nil, func(ergo.Error) {
		require.FailNow(t, "")
	}))

	var err error
	require.False(t, ergo.IsError(&err, func(ergo.Error) {
		require.FailNow(t, "")
	}))

	var E ergo.Error
	err = E
	require.False(t, ergo.IsError(&err, func(ergo.Error) {
		require.FailNow(t, "")
	}))

	err = ergo.New("")
	require.True(t, ergo.IsError(&err, nil))
}

// -----------------------------------------------------------------------------

// IsError: Ensure correct operation with plain error
func TestIsError_PlainError(t *testing.T) {
	err := fmt.Errorf("Plain Error")
	called := false

	require.True(t, ergo.IsError(&err, func(ergoerr ergo.Error) {
		called = true
		require.Nil(t, ergoerr)
	}))
	require.True(t, called)
}

// -----------------------------------------------------------------------------

// IsError: Ensure correct operation with ergo error
func TestIsError_ErgoError(t *testing.T) {
	var err error = ergo.New("Ergo Error")
	called := false

	require.True(t, ergo.IsError(&err, func(ergoerr ergo.Error) {
		called = true
		require.NotNil(t, ergoerr)
		require.Equal(t, err.(ergo.Error), ergoerr)
	}))
	require.True(t, called)
}

// -----------------------------------------------------------------------------
