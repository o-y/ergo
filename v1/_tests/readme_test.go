package ergo_test

import (
	"fmt"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
)

// -----------------------------------------------------------------------------

func ReadmeExampleNewError() error {
	return ergo.New("Error Message")
}

// -----------------------------------------------------------------------------

func ReadmeExampleContextualDataOne() error {
	return ergo.New("Error Message", ergo.AddContext(pp.Map{"Error ID": 42}))
}

func ReadmeExampleContextualDataTwo() error {
	return ergo.New("Error Message", ergo.AddContext(pp.Map{
		"Query Command": "SELECT something FROM somethingelse",
		"Error ID":      42,
	}))
}

// -----------------------------------------------------------------------------

func ReadmeExampleCause() error {
	err, file := openfile()
	if err != nil {
		return ergo.New("Write Failed", ergo.SetCause(err))
	}
	writedata(file)
	return nil
}

func openfile() (error, interface{}) { return nil, nil }
func writedata(interface{})          {}

// -----------------------------------------------------------------------------

func ReadmeExampleWrapOne() error {
	err := dosomething()
	if err != nil {
		return ergo.Wrap(&err)
	}
	return nil
}

func dosomething() error { return nil }

func ReadmeExampleWrapTwo() (err error) {
	defer ergo.Wrap(&err)

	err = dosomething()
	if err != nil {
		return err
	}

	return nil
}

// -----------------------------------------------------------------------------

func ReadmeExamplePubErrOne() error {
	return ergo.New("Error Message", ergo.SetPub("Public Error Message"))
}

func ReadmeExamplePubErrTwo() error {
	return ergo.NewPub("Public Error Message")
}

// -----------------------------------------------------------------------------

func ReadmeExampleSiblings() error {
	err1 := fmt.Errorf("Plain Error")
	err2 := fmt.Errorf("Plain Error 2")
	err3 := ergo.New("Ergo Error")

	return ergo.NewMulti(err1, err2, err3)
}

// -----------------------------------------------------------------------------
