package ergo_test

import (
	"fmt"
	"regexp"
	"runtime"
	"strings"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

// New: Ensure correct stacktrace
func TestNew_StackTrace(t *testing.T) {
	E := ergo.New("")
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 18, "TestNew_StackTrace")
}

// -----------------------------------------------------------------------------

// NewMulti: Ensure correct stacktrace for plain error
func TestNewMulti_CorrectStackForPlainErr(t *testing.T) {
	E := ergo.NewMulti(fmt.Errorf(""))
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 26, "TestNewMulti_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// NewPub: Ensure correct stacktrace for plain error
func TestNewPub_CorrectStackForPlainErr(t *testing.T) {
	E := ergo.NewPub("")
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 34, "TestNewPub_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// SetCause: Ensure correct stacktrace
func TestSetCause_SetsStackTrace(t *testing.T) {
	E := ergo.New("")
	ergo.SetCause(ergo.New("")).Set(E)

	require.NotNil(t, E.Cause())
	stackchecker(t, E.Cause().StackTrace(), "stacktrace_test.go", 43, "TestSetCause_SetsStackTrace")

	// ---

	ergo.SetCause(fmt.Errorf("")).Set(E)

	require.NotNil(t, E.Cause())
	stackchecker(t, E.Cause().StackTrace(), "stacktrace_test.go", 50, "TestSetCause_SetsStackTrace")
}

// -----------------------------------------------------------------------------

// Wrap: Ensure correct stacktrace for plain error
func TestWrap_CorrectStackForPlainErr(t *testing.T) {
	err := fmt.Errorf("")
	E := ergo.Wrap(&err)
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 61, "TestWrap_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// SkipStack: Skips stack correctly
func TestSkipStack(t *testing.T) {
	E := func() ergo.Error {
		return ergo.New("")
	}()
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 70, "")

	ergo.SkipStack(1).Set(E)
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 71, "TestSkipStack")
}

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// Ensures first stack frame matches given (expected) parameters
func stackchecker(t *testing.T, frames []ergo.StackFrame,
	filesuffix string, line int, prettyfunc string,
) {
	idpc, _, idline, _ := runtime.Caller(1)
	idfunc := runtime.FuncForPC(idpc)
	idprettyfunc := regexp.MustCompile(`^.*\.`).ReplaceAllString(idfunc.Name(), "")
	id := fmt.Sprintf("%s:%d", idprettyfunc, idline)

	// ---

	require.NotEmpty(t, frames, id)
	frame := frames[0]

	fail := false

	if filesuffix != "" {
		if !strings.HasSuffix(frame.File(), filesuffix) {
			fail = true
		}
	}

	if line != -1 {
		if line != frame.Line() {
			fail = true
		}
	}

	if prettyfunc != "" {
		if prettyfunc != frame.PrettyFunc() {
			fail = true
		}
		if !strings.HasSuffix(frame.Func(), prettyfunc) {
			fail = true
		}
	}

	if fail {
		require.FailNow(t, "Stacktrace frame doesn't match", "\nAt %s\n---\nExpected filesuffix = %s\nExpected line = %d\nExpected funcsuffix = %s\nActual = %+v",
			id, filesuffix, line, prettyfunc, frame)
	}
}

// -----------------------------------------------------------------------------
