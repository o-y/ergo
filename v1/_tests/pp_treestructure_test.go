package ergo_test

import (
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestPPTreeStructure(t *testing.T) {

	var E ergo.Error
	var N *pp.ErrorNode

	// ---------------------------------------------------------------------------

	require.Nil(t, pp.TreeStructure(nil))

	require.Nil(t, pp.TreeStructure(ergo.Error(nil)))

	// ---------------------------------------------------------------------------

	E = ergo.NewMulti(
		ergo.New("MultiErr #1", ergo.SetCause(ergo.New("MultiErr #1's Cause #1"))),
		ergo.New("MultiErr #2"),
		ergo.New("MultiErr #3", ergo.SetPub("MultiErr #3's Public Error Message")),
	)
	N = pp.TreeStructure(E)
	require.NotNil(t, N)

	require.Equal(t, "MultiErr #1", N.Error)
	require.Equal(t, "", N.PublicError)

	require.Len(t, N.Siblings, 2)
	require.Nil(t, N.Siblings[0].Cause)
	require.Len(t, N.Siblings[0].Siblings, 0)
	require.Equal(t, "MultiErr #2", N.Siblings[0].Error)
	require.Equal(t, "", N.Siblings[0].PublicError)

	require.NotNil(t, N.Cause)
	require.Nil(t, N.Cause.Cause)
	require.Len(t, N.Cause.Siblings, 0)
	require.Equal(t, "MultiErr #1's Cause #1", N.Cause.Error)
	require.Equal(t, "", N.Cause.PublicError)

	require.Equal(t, "MultiErr #3", N.Siblings[1].Error)
	require.Equal(t, "MultiErr #3's Public Error Message", N.Siblings[1].PublicError)

	// ---------------------------------------------------------------------------

	E = ergo.New("Looping Error")
	N = pp.TreeStructure(ergo.NewMulti(E, E))
	require.NotNil(t, N)

	require.Equal(t, "Looping Error", N.Error)
	require.Equal(t, "", N.PublicError)

	require.Len(t, N.Siblings, 0)
	require.Nil(t, N.Cause)

	// ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------
