package ergo_test

import (
	"fmt"
	"regexp"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestPPPublicChain(t *testing.T) {

	var buf string

	// ---------------------------------------------------------------------------

	buf = pp.PublicChain(nil)
	require.Equal(t, "", buf)

	buf = pp.PublicChain((ergo.Error)(nil))
	require.Equal(t, "", buf)

	buf = pp.PublicChain(ergo.New("Test error", ergo.SetCause(fmt.Errorf("Cause 1"))))
	require.True(t, regexp.MustCompile(`Generic error`).MatchString(buf), buf)

	buf = pp.PublicChain(ergo.New("Test Error"))
	require.True(t, regexp.MustCompile(`Generic error`).MatchString(buf), buf)

	buf = pp.PublicChain(ergo.NewPub("Test Error"))
	require.True(t, regexp.MustCompile(`Test Error`).MatchString(buf), buf)

	buf = pp.PublicChain(ergo.NewPub("Test Error", ergo.SetCause(ergo.NewPub("Cause 1"))))
	require.True(t, regexp.MustCompile(`Test Error[\d\D]+Cause 1`).MatchString(buf), buf)

	buf = pp.PublicChain(
		ergo.NewMulti(
			ergo.NewPub("Test Error", ergo.SetCause(ergo.NewPub("Cause 1"))),
			ergo.NewPub("Sibling 1", ergo.SetCause(ergo.NewPub("Sibling's Cause 1"))),
		),
	)
	require.True(t, regexp.MustCompile(`Test Error[\d\D]+Sibling 1[\d\D]+Cause 1`).MatchString(buf), buf)
	require.False(t, regexp.MustCompile(`Sibling's Cause 1`).MatchString(buf), buf)

	// ---------------------------------------------------------------------------

	E := ergo.NewPub("Looping Test Error")
	E = ergo.NewMulti(E, E)
	buf = pp.PublicChain(E)
	require.True(t, regexp.MustCompile(`Looping Test Error`).MatchString(buf), buf)
	require.False(t, regexp.MustCompile(`Looping Test Error[\d\D]+Looping Test Error`).MatchString(buf), buf)

	// ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------
