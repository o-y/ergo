package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

// SetCause: Cause correctly set?
func TestSetCause_SetsCause(t *testing.T) {
	E := ergo.New("")

	ergo.SetCause(ergo.New("Cause 1 (Ergo)")).Set(E)
	require.NotNil(t, E.Cause())
	require.Equal(t, "Cause 1 (Ergo)", E.Cause().Error())

	// ---

	ergo.SetCause(fmt.Errorf("Cause 2 (Plain)")).Set(E)
	require.NotNil(t, E.Cause())
	require.Equal(t, "Cause 2 (Plain)", E.Cause().Error())
}

// -----------------------------------------------------------------------------

// SetPub: Sets public error message correctly?
func TestSetPub(t *testing.T) {
	E := ergo.New("")
	ergo.SetPub("Public Message").Set(E)
	require.Equal(t, "Public Message", E.PublicError())
}

// -----------------------------------------------------------------------------

// New & AddContext: Context set correctly?
func TestAddContext(t *testing.T) {
	E := ergo.New("")
	require.Equal(t, map[string]interface{}{}, E.Context())

	ergo.AddContext(pp.Map{"Key1": "A"}).Set(E)
	require.Equal(t, map[string]interface{}{"Key1": "A"}, E.Context())

	ergo.AddContext(pp.Map{"Key2": "B"}).Set(E)
	require.Equal(t, map[string]interface{}{"Key1": "A", "Key2": "B"}, E.Context())
}

// -----------------------------------------------------------------------------

// SkipStack: Leave at least one frame and doesn't panic
func TestSkipStack_RetainOneAndDontPanic(t *testing.T) {
	E := ergo.New("")
	lastframe := E.StackTrace()[len(E.StackTrace())-1:][0]

	ergo.SkipStack(-1).Set(E)
	ergo.SkipStack(10).Set(E)
	ergo.SkipStack(1).Set(E)
	ergo.SkipStack(0).Set(E)

	require.Equal(t, lastframe, E.StackTrace()[0])
}

// -----------------------------------------------------------------------------
