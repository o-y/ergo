package ergo_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

// New: Ensure basic functionality
func TestNew(t *testing.T) {
	E := ergo.New("Error Message")
	require.NotNil(t, E)

	require.Equal(t, "Error Message", E.Error())
	require.Empty(t, E.PublicError())
	require.Nil(t, E.Cause())
	require.Empty(t, E.Siblings())
	require.WithinDuration(t, time.Now(), E.Timestamp(), 10*time.Second)
}

// -----------------------------------------------------------------------------

// NewPub: Ensure optionSetters are called
func TestNewPub(t *testing.T) {
	E := ergo.NewPub("Public Error Message")
	require.Equal(t, "Public Error Message", E.Error())
	require.Equal(t, E.Error(), E.PublicError())
}

// -----------------------------------------------------------------------------

// NewMulti: Ensure basic functionality
func TestNewMulti(t *testing.T) {
	require.Nil(t, ergo.NewMulti())
	require.Nil(t, ergo.NewMulti(nil))

	E := ergo.NewMulti(fmt.Errorf("Error 1"), ergo.New("Error 2"), ergo.New("Error 3"))
	require.Len(t, E.Siblings(), 2)
	require.Equal(t, "Error 1", E.Error())
	require.Equal(t, "Error 2", E.Siblings()[0].Error())
	require.Equal(t, "Error 3", E.Siblings()[1].Error())
}

// -----------------------------------------------------------------------------

// NewMulti: Ensure nil elements are not added as siblings
func TestNewMulti_NilGapRemoval(t *testing.T) {
	E := ergo.NewMulti(nil, fmt.Errorf("Error 1"), nil, fmt.Errorf("Error 2"), nil)

	require.Len(t, E.Siblings(), 1)
	require.Equal(t, "Error 1", E.Error())
	require.Equal(t, "Error 2", E.Siblings()[0].Error())
}

// -----------------------------------------------------------------------------

// NewMulti: Ensure correct nil and empty
func TestNewMulti_Nil(t *testing.T) {
	require.Nil(t, ergo.NewMulti(nil))
	require.Nil(t, ergo.NewMulti([]error{}...))
}

// -----------------------------------------------------------------------------

type optionSetter func(ergo.Error)

func (r optionSetter) Set(e ergo.Error) { r(e) }

// NewPub: Ensure OptionSetters are called
func TestNewPub_TestOptionSetters(t *testing.T) {
	count := 0
	opt := optionSetter(func(E ergo.Error) {
		count++
	})
	ergo.New("", opt, opt)

	require.Equal(t, 2, count)
}

// -----------------------------------------------------------------------------
