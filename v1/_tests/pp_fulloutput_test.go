package ergo_test

import (
	"regexp"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestPPFullOutput(t *testing.T) {

	var buf string

	// ---------------------------------------------------------------------------

	// test nil
	require.Equal(t, "", pp.FullOutput(nil))

	require.Equal(t, "", pp.PublicChain((ergo.Error)(nil)))

	// ---------------------------------------------------------------------------

	// test output format for one Error
	buf = pp.FullOutput(ergo.New("Test Error", ergo.SetPub("Public Error"), ergo.AddContext(pp.Map{
		"asd": "efg",
	})))
	require.True(t, regexp.MustCompile(`^\[.+ (.+)\]  @.+\n  ".+:[0-9]+"\n  - .+: .+\n\D*$`).MatchString(buf), buf)

	// test output for one Error
	buf = pp.FullOutput(ergo.New("Test Error", ergo.AddContext(pp.Map{
		"asd": "efg",
	})))
	require.True(t, regexp.MustCompile(`Test Error.+TestPPFullOutput[\d\D]+pp_fulloutput_test.go:\d+[\d\D]+asd:.+efg`).MatchString(buf), buf)

	// ---------------------------------------------------------------------------

	// test ordering of Errors
	buf = pp.FullOutput(ergo.NewMulti(
		ergo.New("Multi 1", ergo.SetCause(ergo.New("Multi 1's Cause"))),
		ergo.New("Multi 2"),
	))

	require.True(t, regexp.MustCompile(`[\d\D]+Multi 1[\d\D]+Multi 2[\d\D]+Multi 1's Cause[\d\D]+`).MatchString(buf), buf)

	// ---------------------------------------------------------------------------

	// test if looping is safe

	E := ergo.New("Looping Test Error")
	E = ergo.NewMulti(E, E)
	buf = pp.FullOutput(E)

	require.True(t, regexp.MustCompile(`Looping Test Error`).MatchString(buf), buf)
	require.False(t, regexp.MustCompile(`Looping Test Error[\d\D]+Looping Test Error`).MatchString(buf), buf)

	// ---------------------------------------------------------------------------
}

// -----------------------------------------------------------------------------
