package example_use_cases_test

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
)

// --------------------------------------------------------------------------------

func TestAllInOne(t *testing.T) {
	// Call dummy test function that returns an error
	err := testAllInOne()
	if err == nil {
		return
	}

	// Assume err is an ergo error
	ergoerr, ok := err.(ergo.Error)
	if !ok {
		fmt.Println(err)
		os.Exit(1)
	}

	// Create a human-readable representation (string) of ergoerr
	humanstr := pp.FullOutput(ergoerr)

	// Write human-readable representation to stdoutthat to stdout
	// fmt.Println(humanstr)

	// Construct a go-marshallable struct from the whole ergoerr tree
	errtree := pp.TreeStructure(ergoerr)

	// Marshal the produced struct to json
	jsonbytes, err := json.Marshal(&errtree)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	// Write json to stdout
	// fmt.Println(string(jsonbytes))

	//
	// -- Line below exists to make compiler not complain about unused variable
	_, _ = humanstr, jsonbytes
}

// --------------------------------------------------------------------------------

func testAllInOne() (err error) {
	// Ensure returned error is ergo error
	defer ergo.Wrap(&err)

	// Add contextual data when this function is about to return an ergo error
	defer ergo.IsError(&err, func(ergoerr ergo.Error) {
		ergo.AddContext(pp.Map{
			"time": time.Now().Unix(),
		}).Set(ergoerr)
	})

	err = dummyValidate()
	if err != nil {
		// Wrap the error in an ergo error (unless it's already)
		return ergo.Wrap(&err)
	}

	err = doSomething()
	if err != nil {
		// WrapErr can be used this way as well
		ergo.Wrap(&err)
		return err
	}

	// This error would be wrapped to become ergo error
	return fmt.Errorf("Plain Error")
}

// --------------------------------------------------------------------------------

// This function returns an ergo error that has siblings
func dummyValidate() error {
	// Create an array of dummy validation errors
	return ergo.NewMulti(
		fmt.Errorf("dummyValidate: Validation error 1"),
		fmt.Errorf("dummyValidate: Validation error 2"),
		fmt.Errorf("dummyValidate: Validation error 3"),
	)
}

// --------------------------------------------------------------------------------

// This function returns an ergo error
func doSomething() error {
	err := doAnotherThing()
	if err != nil {
		return ergo.NewPub("Something failed", ergo.SetCause(err))
	}
	return nil
}

// --------------------------------------------------------------------------------

// This function returns a plain error
func doAnotherThing() error {
	return fmt.Errorf("doAnotherThing: Plain Error")
}

// --------------------------------------------------------------------------------
