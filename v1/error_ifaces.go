package ergo

import (
	"time"
)

// -----------------------------------------------------------------------------

// Error can store a public error message, cause, linked errors, contextual data
// and stack trace in addition to a plain error message.
type Error interface {
	// Error message. Can be empty.
	Error() string

	// Public error message. Can be empty.
	PublicError() string

	// Cause of this error. Can be nil.
	Cause() error

	// Linked errors.
	Linked() LinkedErrors

	// Returns the associated context.
	Context() ErrorContext

	// Stack trace from the moment of this error's creation.
	StackTrace() StackTrace

	// Timestamp from the moment of this error's creation.
	Timestamp() time.Time
}

// -----------------------------------------------------------------------------

// Represents the contextual data associated with an Error.
type ErrorContext interface {
	// Returns the contextual data at the specified index.
	Get(index int) (key, value string)

	// Returns the number of contextual data entries.
	Len() int

	// Adds an entry to the context.
	Add(key, value string) ErrorContext

	// Adds entries to the context from m.
	// Note that pp.Map{"key":"value"} works for m.
	AddFrom(m map[string]string) ErrorContext
}

// -----------------------------------------------------------------------------

// Represents the errors that are linked to an Error.
type LinkedErrors interface {
	// Returns the linked error at the specified index.
	Get(index int) error

	// Returns the number of linked errors.
	Len() int

	// Adds linked errors
	Add(...error) LinkedErrors
}

// -----------------------------------------------------------------------------

// Represents the stack trace of an Error.
// Values are in the format returned by runtime.Callers and runtime.FuncForPC.
type StackTrace interface {
	// Returns the number of stack frames. At least one.
	Len() int

	// Returns stack frame from the specified index.
	//   - Path to the source file or "unknown_file" if not known.
	//   - Line number in the source file or -1 if not known.
	//   - Name of the calling function or program counter number if not known.
	Get(index int) (file string, line int, funk string)
}

// -----------------------------------------------------------------------------
