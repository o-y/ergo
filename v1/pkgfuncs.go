package ergo

// -----------------------------------------------------------------------------

// Determines if err implements error and/or Error interfaces.
// Returns true if implements error and non-nil Error if implements Error.
func IsError(err error) (Error, bool) {
	if err == nil {
		return nil, false
	}

	var (
		enherr  Error
		iserror bool
	)

	// test if err implements Error
	if e, ok := err.(Error); ok {
		enherr = e
	}

	// test if err implements error
	if _, ok := err.(error); ok {
		iserror = true
	}

	return enherr, iserror
}

// -----------------------------------------------------------------------------

// Creates an Error from err and sets it's cause to err.
// If err was already an Error, passes it through unmodified.
// Returns nil if err is nil.
func Wrap(err error) Error {

	// check type of err
	enherr, iserror := IsError(err)
	switch {

	case enherr != nil:
		// type = Error. passthrough
		return enherr

	case iserror:
		// type = non-Error
		return New(err.Error()).SetCause(err).SkipStack(1).Err()
	}

	return nil
}

// -----------------------------------------------------------------------------

// Same as Wrap(), but sets the created Error's public error message as well.
// If err is already an Error and has no public error message, a new Error is
// created with NewPub() with the original error as it's cause.
func WrapPub(err error) Error {

	// check type of err
	enherr, iserror := IsError(err)
	switch {

	case enherr != nil:
		// type = Error

		if enherr.PublicError() != "" {
			// passthrough if has public errormsg
			return enherr
		}

		return NewPub(enherr.Error()).SetCause(err).SkipStack(1).Err()

	case iserror:
		// type = non-Error
		return NewPub(err.Error()).SetCause(err).SkipStack(1).Err()
	}

	return nil
}

// -----------------------------------------------------------------------------
