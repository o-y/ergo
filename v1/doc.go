/*

Ergo is an error library for golang which – among other features – enables
retaining the chain of errors from deep function calls, saving their stack
trace along the way.

For introduction and examples, please visit https://github.com/gima/ergo

Subpackage pp

Provides functionality to produce human readable representation as well as
golang-marshallable struct from errors.
Please take a look before implementing your own printers.
*/
package ergo
