package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestWrap(t *testing.T) {
	require.Nil(t, ergo.Wrap(nil))

	err := fmt.Errorf("Error Message")
	E := ergo.Wrap(err)
	require.Equal(t, err.Error(), E.Error())
	require.Equal(t, "", E.PublicError())
	require.Equal(t, err, E.Cause())

	Err := ergo.New("Error Message").Err()
	E = ergo.Wrap(Err)
	require.Equal(t, E, Err)
}

// -----------------------------------------------------------------------------

func TestWrapPub(t *testing.T) {
	require.Nil(t, ergo.WrapPub(nil))

	olde := fmt.Errorf("Error Message")
	E := ergo.WrapPub(olde)
	require.Equal(t, olde.Error(), E.Error())
	require.Equal(t, olde.Error(), E.PublicError())
	require.Equal(t, olde, E.Cause())

	oldE := ergo.New("Error Message").Err()
	E = ergo.WrapPub(oldE)
	require.Equal(t, oldE.Error(), E.Error())
	require.Equal(t, oldE.Error(), E.PublicError())
	require.Equal(t, oldE, E.Cause())
}

// -----------------------------------------------------------------------------

func TestIsError_Nil(t *testing.T) {
	_, ok := ergo.IsError(nil)
	require.False(t, ok)

	var err error
	_, ok = ergo.IsError(err)
	require.False(t, ok)

	var E error
	_, ok = ergo.IsError(E)
	require.False(t, ok)

	err = E
	_, ok = ergo.IsError(err)
	require.False(t, ok)
}

// -----------------------------------------------------------------------------

func TestIsError(t *testing.T) {
	E, ok := ergo.IsError(fmt.Errorf("Plain Error"))
	require.Nil(t, E)
	require.True(t, ok)

	oldE := ergo.New("Error Message").Err()
	E, ok = ergo.IsError(oldE)
	require.Equal(t, E, oldE)
	require.True(t, ok)
}

// -----------------------------------------------------------------------------
