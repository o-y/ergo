package ergo_test

import (
	"fmt"
	"runtime"
	"strings"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

// wrap
// wrappub
// newmulti

func TestStackTrace_NewMulti(t *testing.T) {
	E := ergo.NewMulti(fmt.Errorf(""))
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 18)
}

// -----------------------------------------------------------------------------
/*
// NewMulti: Ensure correct stacktrace for plain error
func TestNewMulti_CorrectStackForPlainErr(t *testing.T) {
	E := ergo.NewMulti(fmt.Errorf(""))
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 26, "TestNewMulti_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// NewPub: Ensure correct stacktrace for plain error
func TestNewPub_CorrectStackForPlainErr(t *testing.T) {
	E := ergo.NewPub("")
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 34, "TestNewPub_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// SetCause: Ensure correct stacktrace
func TestSetCause_SetsStackTrace(t *testing.T) {
	E := ergo.New("")
	ergo.SetCause(ergo.New("")).Set(E)

	require.NotNil(t, E.Cause())
	stackchecker(t, E.Cause().StackTrace(), "stacktrace_test.go", 43, "TestSetCause_SetsStackTrace")

	// ---

	ergo.SetCause(fmt.Errorf("")).Set(E)

	require.NotNil(t, E.Cause())
	stackchecker(t, E.Cause().StackTrace(), "stacktrace_test.go", 50, "TestSetCause_SetsStackTrace")
}

// -----------------------------------------------------------------------------

// Wrap: Ensure correct stacktrace for plain error
func TestWrap_CorrectStackForPlainErr(t *testing.T) {
	err := fmt.Errorf("")
	E := ergo.Wrap(&err)
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 61, "TestWrap_CorrectStackForPlainErr")
}

// -----------------------------------------------------------------------------

// SkipStack: Skips stack correctly
func TestSkipStack(t *testing.T) {
	E := func() ergo.Error {
		return ergo.New("")
	}()
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 70, "")

	ergo.SkipStack(1).Set(E)
	stackchecker(t, E.StackTrace(), "stacktrace_test.go", 71, "TestSkipStack")
}
*/

// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// Ensures first stack frame matches given (expected) parameters
func stackchecker(t *testing.T, stacktrace ergo.StackTrace, expectFileSuffix string, expectFileLine int) {

	// construct string to identify caller when require.X() commands fail
	idpc, _, idline, _ := runtime.Caller(1)
	idfunc := runtime.FuncForPC(idpc)
	idprettyfunc := pp.PrettyFunc(idfunc.Name())
	id := fmt.Sprintf("%s:%d", idprettyfunc, idline)

	// expect at least one stack frame
	require.NotEqual(t, 0, stacktrace.Len(), id)

	// get 0th stack frame
	frameFile, frameFileLine, frameFunc := stacktrace.Get(0)

	fail := false

	// ensure expected file suffix is present in frame
	if !strings.HasSuffix(frameFile, expectFileSuffix) {
		fail = true
	}

	// ensure expected line number matches frame
	if expectFileLine != frameFileLine {
		fail = true
	}

	// expect prettified function name matches caller
	if idprettyfunc != pp.PrettyFunc(frameFunc) {
		fail = true
	}

	if fail {
		require.FailNow(t, "Stacktrace frame doesn't match", "\nAt %s\n---\nExpected filesuffix = %s\nExpected line = %d\nExpected funcsuffix = %s\nActual = (%s,%d,%s(%s))",
			id, expectFileSuffix, expectFileLine, idprettyfunc,
			frameFile, frameFileLine, frameFunc, pp.PrettyFunc(frameFunc))
	}
}

// -----------------------------------------------------------------------------
