package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestLinkedErrors(t *testing.T) {
	E := ergo.New("").Err()
	require.NotNil(t, E.Linked())
	require.Equal(t, 0, E.Linked().Len())

	le1 := fmt.Errorf("")
	le2 := ergo.New("").Err()
	le3 := ergo.New("").Err()

	l := E.Linked().Add(le1)
	require.Equal(t, E.Linked(), l)
	require.Equal(t, 1, E.Linked().Len())
	require.Equal(t, le1, E.Linked().Get(0))

	E.Linked().Add(le2, le3)
	require.Equal(t, 3, E.Linked().Len())
	require.Equal(t, le2, E.Linked().Get(1))
	require.Equal(t, le3, E.Linked().Get(2))
}

// -----------------------------------------------------------------------------
