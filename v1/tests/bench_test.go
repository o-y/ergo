package ergo_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
)

// -----------------------------------------------------------------------------

func BenchmarkErgoNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ergo.New("Error Message").Err()
	}
}

// -----------------------------------------------------------------------------

func BenchmarkFmtErrorf(b *testing.B) {
	for i := 0; i < b.N; i++ {
		fmt.Errorf("Error Message")
	}
}

// -----------------------------------------------------------------------------

func BenchmarkErrorsNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		errors.New("Error Message")
	}
}

// -----------------------------------------------------------------------------
