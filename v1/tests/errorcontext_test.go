package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestErrorContext(t *testing.T) {
	E := ergo.New("").Err()
	require.NotNil(t, E.Context())
	require.Equal(t, 0, E.Context().Len())

	E.Context().Add("K1", "V1").Add("K2", "V2").Add("K3", "V3")
	require.Equal(t, 3, E.Context().Len())
	k0, v0 := E.Context().Get(0)
	k1, v1 := E.Context().Get(1)
	k2, v2 := E.Context().Get(2)

	require.Panics(t, func() {
		E.Context().Get(3)
	})
	require.Equal(t, "K1V1K2V2K3V3", k0+v0+k1+v1+k2+v2)

	E = ergo.New("").Err()
	E.Context().AddFrom(pp.Map{"K1": "V1", "K2": "V2"}).AddFrom(pp.Map{"K3": "V3"})
	require.Equal(t, 3, E.Context().Len())
	k0, v0 = E.Context().Get(0)
	k1, v1 = E.Context().Get(1)
	k2, v2 = E.Context().Get(2)

	require.Panics(t, func() {
		E.Context().Get(3)
	})

	f1, f2, f3 := false, false, false
	if k0+v0 == "K1V1" || k1+v1 == "K1V1" || k2+v2 == "K1V1" {
		f1 = true
	}
	if k0+v0 == "K2V2" || k1+v1 == "K2V2" || k2+v2 == "K2V2" {
		f2 = true
	}
	if k0+v0 == "K3V3" || k1+v1 == "K3V3" || k2+v2 == "K3V3" {
		f3 = true
	}

	msg := fmt.Sprintf("kv0(%s:%s), kv1(%s:%s), kv2(%s:%s)", k0, v0, k1, v1, k2, v2)
	require.True(t, f1, msg)
	require.True(t, f2, msg)
	require.True(t, f3, msg)
}

// -----------------------------------------------------------------------------
