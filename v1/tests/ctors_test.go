package ergo_test

import (
	"fmt"
	"testing"
	"time"

	"github.com/gima/ergo/v1"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestNew(t *testing.T) {
	E := ergo.New("Error Message").Err()
	require.NotNil(t, E)

	require.Equal(t, "Error Message", E.Error())
	require.Empty(t, E.PublicError())
	require.Nil(t, E.Cause())
	/*
		require.NotNil(t, E.StackTrace())
		require.NotEqual(t, 0, E.Linked().Len())
	*/

	require.WithinDuration(t, E.Timestamp(), time.Now(), 5*time.Second)
}

// -----------------------------------------------------------------------------

func TestNewPub(t *testing.T) {
	E := ergo.NewPub("Public Error Message").Err()
	require.Equal(t, "Public Error Message", E.Error())
	require.Equal(t, E.Error(), E.PublicError())
}

// -----------------------------------------------------------------------------

func TestNewMulti(t *testing.T) {
	require.Nil(t, ergo.NewMulti())
	require.Nil(t, ergo.NewMulti(nil))
	require.Nil(t, ergo.NewMulti([]error{}...))

	E := ergo.NewMulti(fmt.Errorf("Error 1"), ergo.New("Error 2").Err(), ergo.New("Error 3").Err())
	require.Equal(t, 2, E.Linked().Len())
	require.Equal(t, "Error 1", E.Error())
	require.Equal(t, "Error 2", E.Linked().Get(0).Error())
	require.Equal(t, "Error 3", E.Linked().Get(1).Error())
}

// -----------------------------------------------------------------------------
