package ergo_test

import (
	"fmt"
	"testing"

	"github.com/gima/ergo/v1"
	"github.com/gima/ergo/v1/pp"
	"github.com/stretchr/testify/require"
)

// -----------------------------------------------------------------------------

func TestComposer_AddContext(t *testing.T) {
	E := ergo.New("").Err()
	require.Equal(t, 0, E.Context().Len())

	E = ergo.New("").AddContext("K1", "V1").Err()
	require.Equal(t, 1, E.Context().Len())
	k, v := E.Context().Get(0)
	require.Equal(t, "K1V1", k+v)

	E = ergo.New("").AddContextFrom(pp.Map{"K1": "V1"}).Err()
	require.Equal(t, 1, E.Context().Len())
	k, v = E.Context().Get(0)
	require.Equal(t, "K1V1", k+v)
}

// -----------------------------------------------------------------------------

func TestComposer_AddLinked(t *testing.T) {
	olde := fmt.Errorf("")
	oldE := ergo.New("").Err()
	E := ergo.New("").AddLinked(olde, oldE).Err()
	require.Equal(t, 2, E.Linked().Len())
	require.Equal(t, olde, E.Linked().Get(0))
	require.Equal(t, oldE, E.Linked().Get(1))
}

// -----------------------------------------------------------------------------

func TestComposer_SetCause(t *testing.T) {
	oldE := ergo.New("").Err()
	E := ergo.New("").SetCause(oldE).Err()
	require.Equal(t, oldE, E.Cause())

	olde := fmt.Errorf("")
	E = ergo.New("").SetCause(olde).Err()
	require.Equal(t, olde, E.Cause())
}

// -----------------------------------------------------------------------------

func TestComposer_SetPub(t *testing.T) {
	E := ergo.New("").SetPub("Public Message").Err()
	require.Equal(t, "Public Message", E.PublicError())
}

// -----------------------------------------------------------------------------

func TestComposer_SkipStackRetainsOne(t *testing.T) {
	E := ergo.New("").
		SkipStack(-1).
		SkipStack(1000).
		SkipStack(1).
		SkipStack(0).
		SkipStack(-1).
		Err()

	require.Equal(t, 1, E.StackTrace().Len())
}

// -----------------------------------------------------------------------------
