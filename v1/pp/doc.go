/*
Pretty-printing (and related) functionality.

Output format stability

Output format of some of the functions in this package is not frozen.
Such functions have this mentioned in their documentation.
Bottom line is that their output can change if a more appropriate format is found.
Treat them as being being human-readable and meant for human consumption.

Looping error structures

Ergo errors can form a loop via cause and linked errors.
All of the functions in this package are protected against such infinite loops.
*/
package pp
