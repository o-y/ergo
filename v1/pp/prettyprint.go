package pp

import (
	"bytes"
	"fmt"
	"regexp"

	"github.com/gima/ergo/v1"
)

// -----------------------------------------------------------------------------

// Returns chain of public error messages starting from err.
// Output looks like following:
//  [Error's Public Message]
//  [Linked Error's Public Message]
//  [Linked Error 2's Public Message]
//  [Cause's Public Message]
//  ...
// Visits the error tree in-order until hitting the first leaf node and stopping there.
// Non-Errors are treated as leaf nodes. Linked errors are visited, but no deeper.
// Skips errors whose public error message is empty.
// Returns an empty string in case no error was encountered or "[Generic error]"
// in case no public error message was are encountered.
//
// The output of this function can change, but will remain - and is intended to be - human readable.
func PublicChain(err error) string {

	// visitfn cbfn calls write output to this buffer
	buf := bytes.NewBuffer(make([]byte, 0, 192))

	// indicates if any error was successfully visited
	visitedSomeError := false

	// start recursion into the first error (given parameter)
	visitfn(err, &visitedSomeError, true,
		func(err error, isErr bool, dive bool, ifn internalVisitFn) interface{} {

			// non-Error error messages are not public
			if !isErr {
				return nil
			}

			E := err.(ergo.Error)

			// print public error (if any)
			if len(E.PublicError()) != 0 {
				buf.WriteByte('[')
				buf.WriteString(E.PublicError())
				buf.WriteByte(']')
				buf.WriteByte('\n')
			}

			// to dive/not to dive
			if !dive {
				return nil
			}

			// dive into linkeds, but not deeper
			for i := 0; i < E.Linked().Len(); i++ {
				ifn(E.Linked().Get(i), false)
			}

			// dive into cause, but not deeper
			ifn(E.Cause(), false)

			return nil
		})

	// check if no error was visited
	if !visitedSomeError {
		return ""
	}

	if buf.Len() == 0 {
		return "[Generic error]"
	}
	return buf.String()
}

// -----------------------------------------------------------------------------

// Returns all error messages reachable from err. For ergo errors, the public error message,
// context, originating file, function and line will be included as well.
// The output tries to take into account the relations of the errors (cause,
// siblings), but the tree-like structure cannot be represented well in text,
// see TreeStructure(). Returns an empty string in case no error was visited.
//
// Output looks like following:
//  [Test Error (Public Error Message)]  @FunctionName
//    "/path/to/file.go:41"
//  [Error's Cause]  @FunctionName
//    "/path/to/file.go:38"
//    - Key: Value
//  [Error Cause's Cause]  @FunctionName
//    "/path/to/file.go:51"
//  ...
//
// The output of this function can change, but will remain - and is intended to be - human consumable.
func FullOutput(err error) string {

	// visitfn cbfn calls write output to this buffer
	buf := bytes.NewBuffer(make([]byte, 0, 384))

	// indicates if any error was successfully visited
	visitedSomeError := false

	// start recursion into the first error (given parameter)
	visitfn(err, &visitedSomeError, true, func(e error, isErr bool, dive bool, ifn internalVisitFn) interface{} {

		if !isErr {
			// non-Error
			fmt.Fprintf(buf, "[%s]  Non-Error. No stack trace available.\n", err.Error())
			return nil
		}

		E := err.(ergo.Error)

		file, line, funk := E.StackTrace().Get(0)

		// print error information
		if len(E.PublicError()) != 0 && E.Error() != E.PublicError() {
			fmt.Fprintf(buf, "[%s (%s)]  @%s\n", E.Error(), E.PublicError(), PrettyFunc(funk))
		} else {
			fmt.Fprintf(buf, "[%s]  @%s\n", E.Error(), PrettyFunc(funk))
		}

		fmt.Fprintf(buf, "  \"%s:%d\"\n", file, line)

		// print error context
		for i := 0; i < E.Context().Len(); i++ {
			k, v := E.Context().Get(i)
			fmt.Fprintf(buf, "  - %s: %+v\n", k, v)
		}

		// param "dive" is ignored, because full output is desired and ifn does
		// nothing with dive

		// dive into siblings
		for i := 0; i < E.Linked().Len(); i++ {
			ifn(E.Linked().Get(i), dive)
		}

		// dive into cause
		ifn(E.Cause(), dive)

		return nil
	})

	// check if no error was visited
	if !visitedSomeError {
		return ""
	}

	return buf.String()
}

// -----------------------------------------------------------------------------

// Returns the full error chain in a tree data structure.
// The data structure is traversible with golang reflection, and hence with
// encoding/json for example.
//
// Note that ergo errors can form a loop by means of cause and siblings.
// When such a loop is detected, the loop will be broken.
// This action loses information, but should be of no concern, since such loops
// have to be intentionally constructed.
func TreeStructure(err error) (_ *ErrorNode, ok bool) {

	// indicates if any error was successfully visited
	visitedSomeError := false

	ret := visitfn(err, &visitedSomeError, true,
		func(err error, isErr bool, dive bool, ifn internalVisitFn) interface{} {

			if !isErr {
				// construct this node from non-Error type
				return &ErrorNode{
					Error:       err.Error(),
					PublicError: "",
					Cause:       nil,
					Linked:      nil,
					Context:     nil,
					Stack:       nil,
				}
			}

			// construct this node from Error type
			E := err.(ergo.Error)

			ret := &ErrorNode{
				Error:       E.Error(),
				PublicError: E.PublicError(),
				Cause:       nil,
				Linked:      nil,
				Context:     nil,
				Stack:       nil,
			}

			// construct linked
			if E.Linked().Len() > 0 {
				ret.Linked = make([]*ErrorNode, 0, E.Linked().Len())

				for i := 0; i < E.Linked().Len(); i++ {
					l := E.Linked().Get(i)
					if x := ifn(l, dive); x != nil {
						ret.Linked = append(ret.Linked, x.(*ErrorNode))
					}
				}
			} else {
				ret.Linked = nil
			}

			// construct cause
			if c := ifn(E.Cause(), dive); c != nil {
				ret.Cause = c.(*ErrorNode)
			}

			// construct context
			if E.Context().Len() > 0 {
				ret.Context = map[string]string{}

				for i := 0; i < E.Context().Len(); i++ {
					k, v := E.Context().Get(i)
					ret.Context[k] = v
				}
			}

			// construct stack
			ret.Stack = make([]StackFrame, E.StackTrace().Len())

			for i := 0; i < E.StackTrace().Len(); i++ {
				file, line, funk := E.StackTrace().Get(i)

				ret.Stack[i] = StackFrame{
					File:       file,
					Line:       line,
					Func:       funk,
					PrettyFunc: PrettyFunc(funk),
				}
			}

			return ret
		})

	if ret == nil {
		return nil, false
	}

	return ret.(*ErrorNode), true
}

// -----------------------------------------------------------------------------

var prettyFuncRe = regexp.MustCompile(`^.*\.`)

// Tries to return only the function name.
// Failing that, returns what was given.
func PrettyFunc(funcName string) string {
	return prettyFuncRe.ReplaceAllString(funcName, "")
}

// -----------------------------------------------------------------------------
