package pp

// -----------------------------------------------------------------------------

// Shorthand for map[string]string. Usable when adding context.
type Map map[string]string

// -----------------------------------------------------------------------------
