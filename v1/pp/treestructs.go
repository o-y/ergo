package pp

// -----------------------------------------------------------------------------

// ErrorNode represents one error in the tree data structure produced by TreeStructure().
type ErrorNode struct {
	// Error message.
	Error string

	// Public error message.
	PublicError string

	// Can be nil.
	Cause *ErrorNode

	// Can be nil.
	Linked []*ErrorNode

	// Can be nil.
	Context map[string]string

	// Can be nil.
	Stack []StackFrame
}

// -----------------------------------------------------------------------------

// Represents one stack trace entry referenced from ErrorNode.
// Values are in the format returned by runtime.Callers and runtime.FuncForPC.
type StackFrame struct {
	// Path to the source file or "unknown_file" if not known.
	File string

	// Line number in the source file or -1 if not known.
	Line int

	// Name of the calling function or program counter number if not known.
	Func string

	// Only the function name or if not known, same as Func.
	PrettyFunc string
}

// -----------------------------------------------------------------------------
