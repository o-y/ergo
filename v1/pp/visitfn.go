package pp

import (
	"github.com/gima/ergo/v1"
)

// -----------------------------------------------------------------------------

type internalVisitFn func(err error, dive bool) interface{}
type visitFn func(err error, isError bool, dive bool, cbfn internalVisitFn) interface{}

// Visits errors starting from err.
// Returns what inner recurse visitor function returns, which returns nil if
// error being visited is nil or was already visited during this function's call.
// Otherwise inner recurse function returns what cbfn returns.
func visitfn(err error, visitedSomeError *bool, dive bool, cbfn visitFn) interface{} {
	// keep record of visited errors so as not to re-visit them
	visitedErrors := map[error]struct{}{}

	// visitor function for errors
	var ifn internalVisitFn
	ifn = func(err error, dive bool) interface{} {

		// ignore nil Errors
		if err == nil {
			return nil
		}

		// signal caller that some error was visited
		if visitedSomeError != nil {
			*visitedSomeError = true
		}

		// test if already visited err
		if _, found := visitedErrors[err]; found {
			// already visited, don't visit again
			return nil
		}

		// mark as visited
		visitedErrors[err] = struct{}{}

		// determine if is Error
		_, isErr := ergo.IsError(err)

		// call caller-provided callback function
		return cbfn(err, isErr, dive, ifn)
	}

	// start recursion into the first error (given parameter)
	return ifn(err, dive)
}

// -----------------------------------------------------------------------------
