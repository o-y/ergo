package ergo

// -----------------------------------------------------------------------------

// Enables composing a new Error by setting it's properties.
// Retrieve the composed Error by calling Err().
type ErrorComposer struct {
	// See the description of contextStruct regarding memory allocation, as it's
	// idea is the same.

	err *errorStruct
}

// -----------------------------------------------------------------------------

// Sets public error message.
func (r ErrorComposer) SetPub(puberrmsg string) ErrorComposer {
	r.err.puberrmsg = puberrmsg
	return r
}

// -----------------------------------------------------------------------------

// Sets cause.
func (r ErrorComposer) SetCause(cause error) ErrorComposer {
	r.err.cause = cause
	return r
}

// -----------------------------------------------------------------------------

// Links errors to this Error.
func (r ErrorComposer) AddLinked(errs ...error) ErrorComposer {
	r.err.Linked().Add(errs...)
	return r
}

// -----------------------------------------------------------------------------

// Adds an entry to the context.
func (r ErrorComposer) AddContext(key, value string) ErrorComposer {
	r.err.Context().Add(key, value)
	return r
}

// -----------------------------------------------------------------------------

// Adds entries to the context from m.
// Note that pp.Map{"key":"value"} works as m.
func (r ErrorComposer) AddContextFrom(m map[string]string) ErrorComposer {
	r.err.Context().AddFrom(m)
	return r
}

// -----------------------------------------------------------------------------

// Removes the given number of entries from the stack trace.
// Will not remove the last stack frame.
func (r ErrorComposer) SkipStack(skip int) ErrorComposer {
	r.err.numskipstack += skip
	return r
}

// -----------------------------------------------------------------------------

// Creates the composed Error.
// ErrorComposer should not be used after calling this function.
func (r ErrorComposer) Err() Error {
	r.err.stack = captureStackTrace(r.err.numskipstack)
	return r.err
}

// -----------------------------------------------------------------------------
