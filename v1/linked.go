package ergo

// -----------------------------------------------------------------------------

// Redirector structure for linked errors method set.
// See the description of contextStruct as it's idea is the same.
type linkedStruct struct {
	err *errorStruct
}

// -----------------------------------------------------------------------------

// See the Linked interface.
func (r linkedStruct) Get(index int) error {
	return r.err.linked_entries[index]
}

// -----------------------------------------------------------------------------

// See the Linked interface.
func (r linkedStruct) Len() int {
	return len(r.err.linked_entries)
}

// -----------------------------------------------------------------------------

// See the Linked  interface.
func (r linkedStruct) Add(errs ...error) LinkedErrors {
	if r.err.linked_entries == nil {
		r.err.linked_entries = errs
		return r
	}

	r.err.linked_entries = append(r.err.linked_entries, errs...)
	return r
}

// -----------------------------------------------------------------------------
