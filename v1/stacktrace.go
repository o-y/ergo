package ergo

import (
	"runtime"
	"strconv"
)

// -----------------------------------------------------------------------------

// See StackTrace interface.
type stackTrace []stackFrame

// -----------------------------------------------------------------------------

// See StackTrace interface.
func (r stackTrace) Len() int {
	return len(r)
}

// -----------------------------------------------------------------------------

// See StackTrace interface.
func (r stackTrace) Get(idx int) (file string, line int, funk string) {
	if idx >= len(r) {
		panic("Index Out of Bounds")
	}
	return r[idx].file, r[idx].line, r[idx].funk
}

// -----------------------------------------------------------------------------

type stackFrame struct {
	file string
	line int
	funk string
}

// -----------------------------------------------------------------------------

// Skip `skip` number of stack frames.
// Skip=0 starts the stack from the caller of this function.
// Returns at least one stack frame.
func captureStackTrace(skip int) StackTrace {

	// no skipping less than 0 element
	if skip < 0 {
		skip = 0
	}

	// capture stack trace
	pcs := make([]uintptr, 20)
	numentries := runtime.Callers(2, pcs)

	// ensure at least one stack frame remains after skipping
	if numentries-skip < 1 {
		skip = numentries - 1
	}

	// ensure skip is valid
	if skip < 0 || skip >= numentries {
		skip = 0
	}

	frames := make(stackTrace, numentries)

	// create stack output
	for i := skip; i < numentries; i++ {
		var file string
		var line int
		var name string

		funk := runtime.FuncForPC(pcs[i] - 1)
		if funk == nil {
			file, line = "unknown_file", -1
			name = strconv.FormatUint(uint64(pcs[i])-1, 1+0xF)
		} else {
			file, line = funk.FileLine(pcs[i] - 1)
			name = funk.Name()
		}

		frames[i-skip] = stackFrame{file: file, line: line, funk: name}
	}

	return frames
}

// -----------------------------------------------------------------------------
