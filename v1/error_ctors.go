package ergo

import (
	"time"
)

// -----------------------------------------------------------------------------

// Creates a new Error with the specified error message.
//
// The Error's public error message will be empty. Cause and Linked will be nil.
// Timestamp will be now. Stack trace will start from the caller of this func.
// See SkipStack().
//
// Use the composer's methods to set public error message, cause, linked errors,
// add context and/or to skip stack frames.
func New(errmsg string) ErrorComposer {
	e := &errorStruct{
		errmsg:         errmsg,
		puberrmsg:      "",
		cause:          nil,
		linked_entries: nil,
		ctx_numentries: 0,
		ctx_more:       nil,
		stack:          nil, // will be filled when ErrorComposer.Err() is called
		timestamp:      time.Now(),
	}
	e.linked.err = e
	e.context.err = e
	return ErrorComposer{err: e}
}

// -----------------------------------------------------------------------------

// Same as New(), but sets public error message as well.
func NewPub(errmsg string) ErrorComposer {
	return New(errmsg).SetPub(errmsg)
}

// -----------------------------------------------------------------------------

// Constructs an Error with linked errors.
//
// Converts the first error to an Error (if it is not already) with New() with
// the original error as it's cause, then adds rest of the given errors (if any)
// as the Error's linked errors.
//
// Returns nil if errs is empty or nil
func NewMulti(errs ...error) Error {

	// must have at least 1 arg which is not nil
	if len(errs) == 0 || errs[0] == nil {
		return nil
	}

	E, _ := IsError(errs[0])

	// convert non-Error to Error
	if E == nil {
		E = New(errs[0].Error()).SetCause(errs[0]).SkipStack(1).Err()
	}

	// add linked errors
	if len(errs) > 1 {
		E.Linked().Add(errs[1:]...)
	}

	return E
}

// -----------------------------------------------------------------------------
