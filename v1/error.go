package ergo

import (
	"time"
)

// -----------------------------------------------------------------------------

// See the Error interface.
// The wish is to avoid as many unnecessary allocations as possible, at the
// expense of a little more memory usage per Error instance. This is also the
// reason why the structure is not so clean.
type errorStruct struct {
	errmsg    string
	puberrmsg string
	cause     error

	linked         linkedStruct
	linked_entries []error

	context        contextStruct
	ctx_numentries int
	ctx_prealloc   [_CTX_NUM_PREALLOC]contextEntry
	ctx_more       []contextEntry

	stack        StackTrace
	numskipstack int

	timestamp time.Time
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) Error() string {
	return r.errmsg
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) PublicError() string {
	return r.puberrmsg
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) Cause() error {
	return r.cause
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) Linked() LinkedErrors {
	return r.linked
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) Context() ErrorContext {
	return r.context
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) StackTrace() StackTrace {
	return r.stack
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r *errorStruct) Timestamp() time.Time {
	return r.timestamp
}

// -----------------------------------------------------------------------------
