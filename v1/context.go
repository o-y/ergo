package ergo

import (
	"fmt"
)

// -----------------------------------------------------------------------------

// This many slots will be preallocated for adding a key-value contextual data
// before a slice allocation is necessary.
const _CTX_NUM_PREALLOC = 3

// -----------------------------------------------------------------------------

// Redirector structure for contextual data method set.
// Because the wish is to avoid unnecessary allocations, this struct cannot be
// a pointer and store data. Thus the data storage has to reside in errorStruct.
// Ugly-ish.
type contextStruct struct {
	err *errorStruct
}

// -----------------------------------------------------------------------------

type contextEntry struct {
	key   string
	value string
}

// -----------------------------------------------------------------------------

// See the Context interface.
func (r contextStruct) Get(index int) (key, value string) {

	fmt.Println("## Get. ctx_more =", r.err.ctx_more)

	if index < _CTX_NUM_PREALLOC {
		// get from preallocated entries

		if index >= r.err.ctx_numentries {
			panic("Index Out of Bounds")
		}

		// index within length, get entry
		e := r.err.ctx_prealloc[index]
		return e.key, e.value
	}

	// get from slice entries (more)

	if index >= _CTX_NUM_PREALLOC+len(r.err.ctx_more) {
		panic("Index Out of Bounds")
	}

	// get requested entry from allocated slice
	e := r.err.ctx_more[index-_CTX_NUM_PREALLOC]
	return e.key, e.value
}

// -----------------------------------------------------------------------------

// See the Context interface.
func (r contextStruct) Len() int {
	return r.err.ctx_numentries
}

// -----------------------------------------------------------------------------

// See the Context interface.
func (r contextStruct) Add(key, value string) ErrorContext {

	if r.err.ctx_numentries < _CTX_NUM_PREALLOC {
		// add to prealloc entries
		r.err.ctx_prealloc[r.err.ctx_numentries] = contextEntry{key, value}

	} else {
		// add to slices

		if r.err.ctx_more == nil {
			r.err.ctx_more = make([]contextEntry, 0, _CTX_NUM_PREALLOC)
		}
		r.err.ctx_more = append(r.err.ctx_more, contextEntry{key, value})
	}

	r.err.ctx_numentries++
	return r
}

// -----------------------------------------------------------------------------

// See the Error interface.
func (r contextStruct) AddFrom(m map[string]string) ErrorContext {

	fmt.Println("## AddFrom", m)
	fmt.Println("## ctx_more =", r.err.ctx_more)

	if _CTX_NUM_PREALLOC-r.err.ctx_numentries < len(m) {
		// alloc space for all elements that will be added
		newlen := len(r.err.ctx_more) + len(m)
		newarr := make([]contextEntry, r.err.ctx_numentries, newlen+_CTX_NUM_PREALLOC)
		copy(newarr, r.err.ctx_more)
		r.err.ctx_more = newarr
	}

	// add elements from map to context
	for k, v := range m {
		r.Add(k, v)
	}

	fmt.Println("## added. ctx_more =", r.err.ctx_more)

	return r
}

// -----------------------------------------------------------------------------
