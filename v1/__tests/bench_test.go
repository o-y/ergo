package ergo_test

import (
	"runtime"
	"testing"

	"github.com/gima/ergo/v1"
)

// -----------------------------------------------------------------------------

/*
func BenchmarkErgo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ergo.New("Error Message").Err()
	}
}
*/

/*
func BenchmarkCaptureStackTrace(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ergo.CaptureStackTrace(0)
	}
}
*/

// -----------------------------------------------------------------------------

var rms1 runtime.MemStats
var rms2 runtime.MemStats
var e ergo.Error
var ec ergo.ErrorComposer

func BenchmarkAddContext(b *testing.B) {

	//runtime.ReadMemStats(&rms1)
	for i := 0; i < b.N; i++ {
		//ec := ergo.New("E2").AddContextEntries(Map{"a": "b", "c": "d"}).AddContext("b", "d").SetCauses(err, err2).Err()
		ec = ergo.New("Error Message")
		ec.AddContext("Key", "Value")
		ec.AddContext("Key", "Value")
		ec.AddContext("Key", "Value")
		e = ec.Err()
	}

	/*
		err = ergo.NewMulti(errs...)
		for i, c := range err.Causes() {
			err.AddContextEntries(pp.Map{
				fmt.Sprintf("SQLDETAIL_%02d", i): err[i].(pg.Error).Detail,
				fmt.Sprintf("SQLWHERE_%02d", i):  err[i].(pg.Error).Where,
			})
		}
	*/

	/*
		fmt.Println(e.Error())
		fmt.Println(e.PublicError())
		fmt.Println(e.Context())
	*/
	/*
		ec.AddContext(map[string]string{"Key": "Value"})
		ec.AddContext(map[string]string{"Key2": "Value2"})
		ec.AddContext(map[string]string{"Key2": "Value2"})
	*/
	/*
		ec.AddContextEntries(
			"key", "value",
		)
		ec.AddContextEntries([]string{}...)
	*/
	/*
		E.SetContextEntry("key1", "value1")
		E.SetContextEntry("key2", "value2")
		E.SetContextEntry("key3", "value3")
		E.SetContextEntry("key4", "value4")
	*/
	/*
		for i := 0; i < E.StackTrace().Length(); i++ {
			file, line, funk := E.StackTrace().Get(i)
			fmt.Println(file, line, funk)
		}
	*/

	//asde = make([]byte, 1024)
	/*
		runtime.ReadMemStats(&rms2)
		fmt.Printf("\nHeapAlloc:%d StackInUse:%d StackSys:%d Alloc:%d TotalAlloc:%d\n",
			rms2.HeapAlloc-rms1.HeapAlloc,
			rms2.StackInuse-rms1.StackInuse,
			rms2.StackSys-rms1.StackSys,
			rms2.Alloc-rms1.Alloc,
			rms2.TotalAlloc-rms1.TotalAlloc)
	*/
}

// -----------------------------------------------------------------------------

/*
func BenchmarkIsError(b *testing.B) {
	for i := 0; i < b.N; i++ {
		if ergoerr, ok := ergo.IsError(e); ok && ergoerr != nil {
			n++
		}
	}
}
*/

// -----------------------------------------------------------------------------

/*
func BenchmarkErrorsNew(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e = errors.New("Asd")
	}
}
*/
// -----------------------------------------------------------------------------

/*
func BenchmarkReturnErgo(b *testing.B) {
	for i := 0; i < b.N; i++ {
		e2 = ret()
	}
}

func ret() error {
	return E
}
*/

// -----------------------------------------------------------------------------

/*
var q map[string]interface{}

func xBenchmarkMapCreation(b *testing.B) {
	for i := 0; i < b.N; i++ {
		q = map[string]interface{}{}
	}
}

// -----------------------------------------------------------------------------
*/
