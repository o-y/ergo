package main

import (
	"fmt"
	"runtime"
)

var rms1 runtime.MemStats
var rms2 runtime.MemStats

var gstring string

var m = map[int]int{}

func main() {
	runtime.ReadMemStats(&rms1)

	for i := 0; i < 10240; i++ {
		m[i] = i + 1
	}

	runtime.ReadMemStats(&rms2)
	fmt.Printf("\nHeapAlloc:%d StackInUse:%d StackSys:%d Alloc:%d TotalAlloc:%d\n",
		rms2.HeapAlloc-rms1.HeapAlloc,
		rms2.StackInuse-rms1.StackInuse,
		rms2.StackSys-rms1.StackSys,
		rms2.Alloc-rms1.Alloc,
		rms2.TotalAlloc-rms1.TotalAlloc)
}
