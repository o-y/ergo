package main_test

import (
	"strconv"
	"testing"
)

var m = map[int]string{}

func BenchmarkTestMap(b *testing.B) {
	for i := 1; i < b.N; i++ {
		for i := 0; i < 10240; i++ {
			m[i] = strconv.Itoa(i)
		}
	}
}
