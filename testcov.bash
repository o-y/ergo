#!/bin/bash

# test that all code compiles
go build -v ./v1/... || exit 1

# define packages that need testing
packages=( \
  "github.com/gima/ergo/v1/tests" \
)

# default to no error
ERROR=0

echo "mode: count" > cover.prof

# loop the packages
for pkg in "${packages[@]}"; do

  # run testing and coverage analysis
  go test -v -covermode=count -coverprofile=cover_tmp.prof -coverpkg ./v1/... -cover "$pkg"
  
  # test for failure
  if [ $? -ne 0 ]; then
    # failed. show error and set to fail later, but let other packages be tested as well
    echo "Failed testing: $pkg" > /dev/stderr
    ERROR=1
  fi
  
  # skip tests that output nothing
  if [ ! -f cover_tmp.prof ]; then
    continue
  fi
  
  # test success. append coverage to combined coverage analysis file
  tail -n +2 cover_tmp.prof >> cover.prof || die "Unable to append coverage for: $pkg"
done

# if testing was not success, terminate script with non-zero exit code to indicate that
if [ $ERROR -ne 0 ]; then exit 1; fi
