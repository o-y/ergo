##### Please note:

* The **API is not frozen**, because the project is [requesting comments](https://github.com/gima/ergo/wiki/RFC-Request-for-comments-wrt-design) about the design.

---

# Ergo

```text
"github.com/gima/ergo/v1"
```

Ergo errors contain more information than plain textual errors, which makes them more useful when investigating problems. All errors record the location and time when the error was created.

This library can be used stand-alone perfectly fine, but it really shines when used in concert with distributed tracing where a top level error handler could tie the error and it's contextual data with the already-logged trace.

[![godoc](https://godoc.org/github.com/gima/ergo/v1?status.png)](https://godoc.org/github.com/gima/ergo/v1)
[![Build Status](https://travis-ci.org/gima/ergo.svg?branch=master)](https://travis-ci.org/gima/ergo)
[![Coverage Status](https://coveralls.io/repos/gima/ergo/badge.svg?branch=master)](https://coveralls.io/r/gima/ergo?branch=master)
[![License: MIT](https://img.shields.io/badge/%E2%9C%93-MIT%20license-4cc61e.svg?style=flat)](http://choosealicense.com/licenses/mit/)


## New error:
There is no primary use case, but we have to start from somewhere:

```go
return ergo.New("Error Message")
```
This just created a new Ergo error which [can be used](https://godoc.org/github.com/gima/ergo/v1#Error) to read the enriched error data. Stack trace among others.


## Contextual data:

An Ergo error can also have contextual data. You have probably used something like `return fmt.Errorf("Error %d happened", 42)` whose contextual data cannot be easily extracted, because it's not structured.

Ergo errors provide a structured way to to add contextual data:
```go
return ergo.New("Error Message", ergo.AddContext(pp.Map{"Error ID": 42}))
```

Which admittedly is a bit more verbose, but can be broken down in different ways. For example:

```go
return ergo.New("Error Message", ergo.AddContext(pp.Map{
	"Query Command": "SELECT something FROM somethingelse",
	"Error ID":      42,
}))
```
This hopefully better showcased the usefulness of structured contextual data.


## Cause – Blame another error:

An Ergo error can be created from nothing or it can be said to have been *caused* by another error. An example would be a write operation being unable to proceed, because a file could not be opened:

```go
err, file := openfile()
if err != nil {
	return ergo.New("Write Failed", ergo.SetCause(err))
}
writedata(file)
return nil
```
Now the returned Ergo error will have it's cause set to the error returned from the fictional openfile(). The cause can be retrieved by calling `err.Cause()`.


## Wrap an existing error:

Sometimes a function returns an error that would benefit from an added stack trace, but creating it with ergo.New would be repetitious.

Wrapping an error works like this:
```go
err := dosomething()
if err != nil {
	return ergo.Wrap(&err)
}
return nil
```
Now the returned error is an Ergo error with the original error's message.

A more advanced use case follows. This ensures that all of the returned errors are indeed Ergo errors:
```go
func SomeFunction() (err error) {
defer ergo.Wrap(&err)

err = dosomething()
if err != nil {
	return err
}

return nil
}
```
In this example the magic-doer is the golang's defer statement. The defer statement allows some code to be run when the function exits. Normally a function's return value doesn't have a name, but when it has, the return value can be overridden by the deferred function.

And because in this example the deferred Wrap function call is given a *pointer* to the named return value (err), the deferred Wrap can overwrite the return value when it's called at the end of the function.

Hopefully it is now clearer why Wrap takes a pointer to an error.


## Public error message:

If your program responds to end-users in some way, it could be a security risk to display the error message to the end-user. For this reason an Ergo error carries a public error message as well. By default this message is empty, but can be manually set to a message that can be safely displayed to a user.
```go
return ergo.New("Error Message", ergo.SetPub("Public Error Message"))
```

If all you want is to create a new error whose error message and public error message are the same, there's a shortcut:

```go
return ergo.NewPub("Public Error Message")
```
Which works the same way and takes the same parameters as ergo.New, but sets the public error message as well.


## Siblings – "an array of errors":

"Returning multiple validation errors in an array, leaving the caller to parse them together somehow." Sounds familiar? Ergo can help with this a bit. By combining multiple errors, it's possible to return a group of errors, while still remaining compatible with the built-in error interface:
```go
err1 := fmt.Errorf("Plain Error")
err2 := fmt.Errorf("Plain Error 2")
err3 := ergo.New("Ergo Error")

return ergo.NewMulti(err1, err2, err3)
```
The returned error is an Ergo error with err1's error message and err2 and err3 as it's siblings. Mix and match ergo and non-ergo errors, all are converted to Ergo errors.


## More documentation?

It doesn't get more complicated than this, but for a full understanding, please take a look at the [godoc](https://godoc.org/github.com/gima/ergo/v1). It is also recommended to take a look at the [use cases](https://github.com/gima/ergo/tree/master/v1/example_use_cases) in which ergo can be applied.


## Pretty-printing:

The provided pretty-printer package can produce human readable representation as well as golang-marshallable struct from ergo errors. The marshallable struct can be used to manually produce/export some custom format or json, for example.

Example output of "FullOutput" pretty-printer.
```text
[Test Error (Public Error Message)]  @FunctionName
  "/path/to/file.go:41"
[Error's Cause]  @FunctionName
  "/path/to/file.go:38"
[Error Cause's Cause]  @FunctionName
  "/path/to/file.go:51"
  - Key: Value
...
```

Example output of "PublicChain" pretty-printer.
```text
[Error's Public Message]
[Sibling's Public Message]
[Sibling 2's Public Message]
[Cause's Public Message]
...
```


## See Also
Many other golang error libraries to choose from:  
[GitHub Search for: "error" + "language:go" ](https://github.com/search?utf8=%E2%9C%93&q=error+language%3AGo&type=Repositories&ref=advsearch&l=Go)  


## License

[MIT License](http://choosealicense.com/licenses/mit/) | Authoritative=LICENSE.txt  
Mention of origin would be appreciated.

*go, golang, ergo, error library, dapper, zipkin, distributed tracing*
