##### Please note:

* The **API is not frozen**, because the project is incubating.  
You can affect the design by [joining the chat](https://gitter.im/gima/ergo) or by some other means making your voice heard (GitHub issues?).

---

# Overview

[![godoc](https://godoc.org/github.com/gima/ergo/v1?status.png)](https://godoc.org/github.com/gima/ergo/v1)
[![Build Status](https://travis-ci.org/gima/ergo.svg?branch=master)](https://travis-ci.org/gima/ergo)
[![Coverage Status](https://coveralls.io/repos/gima/ergo/badge.svg?branch=master)](https://coveralls.io/r/gima/ergo?branch=master)
[![License: MIT](https://img.shields.io/badge/%E2%9C%93-MIT%20license-4cc61e.svg?style=flat)](http://choosealicense.com/licenses/mit/)
[![Project Chat](https://badges.gitter.im/Join%20Chat.svg)](https://gitter.im/gima/ergo)

Ergo is an error library for golang which – among other features – enables retaining the chain of errors from deep function calls, saving their stack trace along the way.

```text
"github.com/gima/ergo/v1"
```


## Features

In addition to a plain error message:

* Public error message – *For displaying to users when the error message contains sensitive data.*
* Cause – *To blame another error; another error was the cause of this error.*
* Linked errors – *To tie multiple errors together; an array of errors.*
* Context – *To relay structured data with the error.*
* Stack trace – *Origin of the error is recorded.*
* Timestamp – *Creation time of the error is recorded.*

This library aims to be efficient by avoiding unnecessary memory allocations, and bases itself around an interface, thus making inter-operability possible.


# Documentation

Below you can find explanations for the functionalities provided by this library. For reference documentation, please visit the [godoc](https://godoc.org/github.com/gima/ergo/v1). It can also be of use to take a look at some of the [use cases](https://github.com/gima/ergo/tree/master/v1/_example_use_cases) of this library.

## New error:

It is quite simple to create a new *Error*:

```go
{{.ReadmeExampleNewError}}
```
This just created a new *Error*. It provides a [rich set of functionality](https://godoc.org/github.com/gima/ergo/v1#Error) such as the stack trace, for example.


## Contextual data:

An *Error* can also have contextual data. You have probably used something like `return fmt.Errorf("Error %d happened", 42)` whose contextual data cannot be easily extracted, because it's not structured.

This library's *Errors* are able to carry structured contextual data:
```go
{{.ReadmeExampleContextualDataOne}}
```

Which admittedly is a bit more verbose, but can be broken down in different ways. For example:

```go
{{.ReadmeExampleContextualDataTwo}}
```


## Cause – Blame another error:

An *Error* can be created from nothing, or it can be said to have been *caused* by another error. An example would be a write operation being unable to proceed, because a file could not be opened:

```go
{{.ReadmeExampleCause}}
```
Now the returned *Error* will have it's cause set to the error returned from the fictional openfile(). The cause can be retrieved by calling `err.Cause()`.


## Wrap an existing error:

Sometimes a function returns an error that would benefit from an added stack trace, but creating it with ergo.New would be repetitious.

Wrapping an error works like this:
```go
{{.ReadmeExampleWrapOne}}
```
Now the returned error is an *Erro*r with the original error's message.

A more advanced use case follows. This ensures that all of the returned errors are indeed *Errors*:
```go
func SomeFunction() (err error) {
{{.ReadmeExampleWrapTwo}}
}
```
In this example the magic-doer is the golang's defer statement. The defer statement allows some code to be run when the function exits. Normally a function's return value doesn't have a name, but when it has, the return value can be overridden by the deferred function.


## Public error message:

If your program responds to end-users in some way, it could be a security risk to display the error message to the end-user. For this reason *Errors* can have a public error message as well. By default this message is empty, but can be manually set to a message that can be safely displayed to a user.
```go
{{.ReadmeExamplePubErrOne}}
```

If all you want is to create a new error whose error message and public error message are the same, there's a shortcut:

```go
{{.ReadmeExamplePubErrTwo}}
```


## Linked errors – "an array of errors":

"*Returning multiple validation errors in an array, leaving the caller to parse them together somehow.*" Sounds familiar? This library can help with this a bit. By combining multiple errors, it's possible to return a group of errors, while still remaining compatible with the built-in error interface:
```go
{{.ReadmeExampleSiblings}}
```
The returned *Error* will have err1's error message and err2 and err3 linked to it.


## Pretty-printing:

The provided pretty-printer package can produce a human readable representation as well as golang-marshallable struct from errors.

Example output from the "FullOutput" pretty-printer.
```text
[Test Error (Public Error Message)]  @FunctionName
  "/path/to/file.go:41"
[Error's Cause]  @FunctionName
  "/path/to/file.go:38"
[Error Cause's Cause]  @FunctionName
  "/path/to/file.go:51"
  - Key: Value
...
```

Example output from the "PublicChain" pretty-printer.
```text
[Error's Public Message]
[Linked Errors's Public Message]
[Linked Errors 2's Public Message]
[Cause's Public Message]
...
```


# Efficiency

The aim has been to optimize the memory allocations to be as low as possible for each new *Error*. Currently creating a new *Error* causes three allocations. Of note is also another optimization, which allows adding three contextual data entries before an allocation takes place.

```text
BenchmarkErgoNew          300000              4865 ns/op             528 B/op          3 allocs/op
BenchmarkFmtErrorf       5000000               314 ns/op              32 B/op          2 allocs/op
BenchmarkErrorsNew      20000000                90.2 ns/op            16 B/op          1 allocs/op
```


# Versioning

The subdirectories (v1, ..) denote a stable API version which will not change in backwards-incompatible ways. It is nevertheless recommended that this library be vendored into the project using it, as error handling is such an integral part of a program's operation and human errors can happen.


# Noteworthy

### Distributed tracing

For the interested, this library can used in concert with distributed tracing, where a top-level error handler can tie the error and it's contextual data with the already-logged trace.


### Alternatives

Many other golang error libraries to choose from:  
[GitHub Search for: "error" + "language:go" ](https://github.com/search?utf8=%E2%9C%93&q=error+language%3AGo&type=Repositories&ref=advsearch&l=Go)  


# License MIT

http://choosealicense.com/licenses/mit/  
Mention of origin would be appreciated.

*dapper, zipkin*
